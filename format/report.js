$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:features/selenium/form_submission.feature");
formatter.feature({
  "name": "Submitting the user details form",
  "description": "   As an user interested in sharing my personal details for selenium\n   I want to fill in the personal details\n   So that I can have my details shared to the trainer",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Enter only required fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters the required fields \"\u003cfirst_name\u003e\" \"\u003clast_name\u003e\" \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "the form should be submitted",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "first_name",
        "last_name",
        "email"
      ]
    },
    {
      "cells": [
        "Sam",
        "joseph",
        "sam@test.com"
      ]
    },
    {
      "cells": [
        "Jack",
        "Tan",
        "jack@test.com"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter only required fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters the required fields \"Sam\" \"joseph\" \"sam@test.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters_the_required_fields(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the form should be submitted",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_form_should_be_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter only required fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters the required fields \"Jack\" \"Tan\" \"jack@test.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters_the_required_fields(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the form should be submitted",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_form_should_be_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Enter all required fields and non required fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters \"\u003cfirst_name\u003e\" \"\u003clast_name\u003e\" \"\u003cemail\u003e\" \"\u003cphone\u003e\" \"\u003ccomments\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "the form should be submitted",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "first_name",
        "last_name",
        "email",
        "phone",
        "comments"
      ]
    },
    {
      "cells": [
        "Tim",
        "Peter",
        "tim@test.com",
        "12345",
        "testing first user"
      ]
    },
    {
      "cells": [
        "Ray",
        "Jas",
        "ray@test.com",
        "6789",
        "testing second user"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter all required fields and non required fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters \"Tim\" \"Peter\" \"tim@test.com\" \"12345\" \"testing first user\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters(String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the form should be submitted",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_form_should_be_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter all required fields and non required fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters \"Ray\" \"Jas\" \"ray@test.com\" \"6789\" \"testing second user\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters(String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the form should be submitted",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_form_should_be_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Enter invalid data in the form fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters invalid \"\u003cfirst_name\u003e\" \"\u003clast_name\u003e\" \"\u003cemail\u003e\" \"\u003cphone\u003e\" \"\u003ccomments\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "the validation error \"\u003cerror\u003e\" should be thrown at \"\u003cerror_element\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "first_name",
        "last_name",
        "email",
        "phone",
        "comments",
        "error_element",
        "error",
        "Test_desc"
      ]
    },
    {
      "cells": [
        "Timaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "Peter",
        "tim@test.com",
        "12345",
        "comments 123",
        "i.err.571059195",
        "First Name is too long",
        "Lengthy first name"
      ]
    },
    {
      "cells": [
        "Ray",
        "Jas",
        "dummy",
        "6789",
        "comments 456",
        "i.err.1080100805",
        "Invalid Email Address",
        "Invalid email"
      ]
    },
    {
      "cells": [
        "Sue",
        "John",
        "sue@test.com",
        "phone",
        "comments 789",
        "i.err.8127453",
        "Phone field cannot have characters",
        "Invalid Phone"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter invalid data in the form fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters invalid \"Timaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\" \"Peter\" \"tim@test.com\" \"12345\" \"comments 123\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters_invalid(String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the validation error \"First Name is too long\" should be thrown at \"i.err.571059195\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_validation_error_should_be_thrown(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter invalid data in the form fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters invalid \"Ray\" \"Jas\" \"dummy\" \"6789\" \"comments 456\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters_invalid(String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the validation error \"Invalid Email Address\" should be thrown at \"i.err.1080100805\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_validation_error_should_be_thrown(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is on the data form",
  "keyword": "Given "
});
formatter.match({
  "location": "SeleniumSteps.the_user_is_on_the_data_form()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Enter invalid data in the form fields",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "the user enters invalid \"Sue\" \"John\" \"sue@test.com\" \"phone\" \"comments 789\"",
  "keyword": "When "
});
formatter.match({
  "location": "SeleniumSteps.the_user_enters_invalid(String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the validation error \"Phone field cannot have characters\" should be thrown at \"i.err.8127453\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SeleniumSteps.the_validation_error_should_be_thrown(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});