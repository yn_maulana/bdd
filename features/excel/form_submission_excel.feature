Feature: Submitting the user details form 
	As an user interested in trading
   I want to fill in the personal details
   So that I can open an acocunt
   

Background: 
	Given the user is on the data form page 
	
Scenario Outline: Enter all required fields and non required fields 
#Given the user is on the data form 
	When the user enters the required and non required fields based on "<test_data_id>" 
	Then the user form should be submitted 
	Examples: 
		| test_case                                       | test_data_id|
		| Adding all required & optional fields           | 1           |
		| Adding all required and optional phone field    | 2           |
		
		
Scenario Outline: Enter all required fields and non required fields 
#Given the user is on the data form 
	When the user enters the required and non required fields based on "<test_data_id>" 
	Then the user form should be submitted 
	Examples: 
		| test_case                                       | test_data_id|
		| Adding all required & optional fields           | 1           |
		| Adding all required and optional phone field    | 2           |  

		
Scenario Outline: Enter invalid data in the form fields 
#Given the user is on the data form 
	When the user enters invalid data based on "<test_data_id>" 
	Then the corresponding validation error should be thrown based on "<test_data_id>" 
	Examples: 
		| test_case                              | test_data_id|
		| First name field longer than the limit | 1           |
		| Entering invalid email address         | 2           |
		| Entering invalid phone number          | 3           |
		
