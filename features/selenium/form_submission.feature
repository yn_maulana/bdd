Feature: Submitting the user details form 
   As an user interested in sharing my personal details for selenium
   I want to fill in the personal details
   So that I can have my details shared to the trainer

  Background:
   Given the user is on the data form    
   
Scenario Outline: Enter only required fields 
	#Given the user is on the data form 
	When the user enters the required fields "<first_name>" "<last_name>" "<email>" 
	Then the form should be submitted 
	Examples: 
		|first_name | last_name | email         |
		|Sam        | joseph    | sam@test.com  |
		|Jack       | Tan       | jack@test.com |
		
		
Scenario Outline: Enter all required fields and non required fields 
	#Given the user is on the data form 
	When the user enters "<first_name>" "<last_name>" "<email>" "<phone>" "<comments>" 
	Then the form should be submitted 
	Examples: 
		|first_name | last_name | email         | phone | comments           |
		|Tim        | Peter     | tim@test.com  | 12345 | testing first user |
		|Ray        | Jas       | ray@test.com  | 6789  | testing second user|	
		
		
Scenario Outline: Enter invalid data in the form fields 
	#Given the user is on the data form 
	When the user enters invalid "<first_name>" "<last_name>" "<email>" "<phone>" "<comments>" 
	Then the validation error "<error>" should be thrown at "<error_element>" 
	Examples: 
		|first_name                                | last_name | email         | phone | comments           | error_element     |error                              | Test_desc          |
		|Timaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa | Peter     | tim@test.com  | 12345 | comments 123       | i.err.571059195   |First Name is too long             |Lengthy first name  |
		|Ray                                       | Jas       | dummy         | 6789  | comments 456       | i.err.1080100805  |Invalid Email Address              | Invalid email      |	  
		|Sue                                       | John      | sue@test.com  | phone | comments 789       | i.err.8127453     |Phone field cannot have characters | Invalid Phone      |
		
		
		
		
	  