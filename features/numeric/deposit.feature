Feature: Deposit money in to an User account 

Scenario: Depositing money into User's account should add money in to the Users' account balance 
	Given an User has no money in the account 
	When $100 is deposited in to the account 
	Then the balance should be $100