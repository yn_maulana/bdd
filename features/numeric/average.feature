Feature: Average of two numbers 
   As an user doing the accounting task
   I want to find the average of given numbers
   So that I can use the value for accounting purposes
   
   
Scenario: The average of two numbers 
	Given the user is calculating the average of two numbers 
	When the user enters 3 and 5 
	Then the result should be 4 
	
	
Scenario: The average of two numbers that gives decimal output 
	Given the user is calculating the average of two numbers 
	When the user enters 2 and 5 
	Then the result should be 3.5 
	
	
Scenario Outline: average of given numbers 
	Given the user is calculating the average of two numbers
	When the user enters <value_1> and <value_2> 
	Then the result should be <expected> 
	Examples: 
		|value_1 | value_2 | expected |
		|3       | 3       | 3        |
		|2       | 6       | 4        |
		|6       | 4       | 5        |
		|7       | 8       | 7.5      |
		
    