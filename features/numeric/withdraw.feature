Feature: Withdraw money from an User account 

Scenario: Withdraw money from an User's account should deduct money from the Users' account balance 
	Given an User has $100 money in the account 
	When 50$ is withdrawn from the account 
	Then the balance should be $50