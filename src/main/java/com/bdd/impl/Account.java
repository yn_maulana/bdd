package com.bdd.impl;

public class Account {

	float balance;
	
	public float getBalance() {
		return balance;
	}

	public void depositAmount(float deposit) {
		this.balance = balance + deposit;
	}
	
	public void withdrawAmount(float deposit) {
		this.balance = balance - deposit;
	}
	
}
