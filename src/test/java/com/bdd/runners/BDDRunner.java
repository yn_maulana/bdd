package com.bdd.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="features/numeric", 
				 glue="com.bdd.stepdefinitions", 
				 tags = {"not @ignore"},
				 plugin={"pretty","html:format"})
public class BDDRunner {

}
