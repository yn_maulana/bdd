package com.bdd.selenium.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SeleniumSteps {

	WebDriver driver;
	WebDriverWait wait;
	
	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "driver\\chromedriver\\chromedriver.exe");
		 driver = new ChromeDriver();
		 wait = new WebDriverWait(driver, 20);
		 
	}

	@After
	public void tearDownChromeDriver() {
		driver.quit();
	}
	
	
	
	@Given("the user is on the data form")
	public void the_user_is_on_the_data_form() {
		driver.get("https://goo.gl/forms/qZXOQ5i7DCfJWSK93");
	}

	@When("the user enters {string} {string} {string} {string} {string}")
	public void the_user_enters(String firstName, String lastName, String email, String phone, String comments) {
		driver.findElement(By.name("entry.652836268")).sendKeys(firstName);
		driver.findElement(By.name("entry.2080829351")).sendKeys(lastName);
		driver.findElement(By.name("entry.740082629")).sendKeys(email);
		driver.findElement(By.name("entry.186612323")).sendKeys(phone);
		driver.findElement(By.name("entry.1742399288")).sendKeys(comments);
		driver.findElement(By.className("quantumWizButtonPaperbuttonLabel")).click();
	}

	@When("the user enters the required fields {string} {string} {string}")
	public void the_user_enters_the_required_fields(String firstName, String lastName, String email) {
		driver.findElement(By.name("entry.652836268")).sendKeys(firstName);
		driver.findElement(By.name("entry.2080829351")).sendKeys(lastName);
		driver.findElement(By.name("entry.740082629")).sendKeys(email);
		driver.findElement(By.className("quantumWizButtonPaperbuttonLabel")).click();
	}
	
	@Then("the form should be submitted")
	public void the_form_should_be_submitted() {
		assertEquals("Tanggapan Anda telah direkam.", wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("freebirdFormviewerViewResponseConfirmationMessage"))).getText());
   
	}
	
	@When("the user enters invalid {string} {string} {string} {string} {string}")
	public void the_user_enters_invalid(String firstName, String lastName, String email, String phone, String comments) {
		driver.findElement(By.name("entry.652836268")).sendKeys(firstName);
		driver.findElement(By.name("entry.2080829351")).sendKeys(lastName);
		driver.findElement(By.name("entry.740082629")).sendKeys(email);
		driver.findElement(By.name("entry.186612323")).sendKeys(phone);
		driver.findElement(By.name("entry.1742399288")).sendKeys(comments);
	}

	@Then("the validation error {string} should be thrown at {string}")
	public void the_validation_error_should_be_thrown(String error, String element) {
		assertEquals(error, wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element))).getText());
	}
}
