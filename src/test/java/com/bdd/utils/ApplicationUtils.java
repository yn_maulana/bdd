package com.bdd.utils;

import java.io.IOException;
import java.util.List;

public class ApplicationUtils {

	 private static ApplicationUtils INSTANCE;
	 
	 private ApplicationUtils() { 
		 
	 }
	     
	 public static ApplicationUtils getInstance() {
	        if(INSTANCE == null) {
	            INSTANCE = new ApplicationUtils();
	        }	         
	        return INSTANCE;
	 }
	 
	 public ExcelReader getExcelReader(String fileName, String sheetName){
		 return new ExcelReader.ExcelReaderBuilder()
			.setFileLocation("testdata"+"/" + fileName)
			.setSheet(sheetName)
			.build();	
	 }

	public List<List<String>> getListOfSheetData(String fileName, String sheetName) {
		try {
			return this.getExcelReader(fileName, sheetName).getListOfSheetData();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	 
	 
	
}
