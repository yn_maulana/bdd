package com.bdd.excel.stepdefinitions;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bdd.utils.ApplicationUtils;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SeleniumExcelSteps {

	WebDriver driver;
	WebDriverWait wait;
	
	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver\\chromedriver.exe");
		 driver = new ChromeDriver();
		 wait = new WebDriverWait(driver, 20);
		 
	}

	@After
	public void tearDownChromeDriver() {
		driver.quit();
	}
	

	@Given("the user is on the data form page")
	public void the_user_is_on_the_data_form_page() {
		driver.get("https://goo.gl/forms/qZXOQ5i7DCfJWSK93");
	}
	
	@When("the user enters the required fields based on {string}")
	public void the_user_enters_the_required_fields_based_on(String rowIndex) {
		List<List<String>> dataListMaster = ApplicationUtils.getInstance()
				.getListOfSheetData("form.xlsx", "required");
		List<String> dataList = dataListMaster.get(Integer.parseInt(rowIndex));
		driver.findElement(By.name("entry.652836268")).sendKeys(dataList.get(0));
		driver.findElement(By.name("entry.2080829351")).sendKeys(dataList.get(1));
		driver.findElement(By.name("entry.740082629")).sendKeys(dataList.get(2));
		driver.findElement(By.className("quantumWizButtonPaperbuttonLabel")).click();
	}

	@Then("the user form should be submitted")
	public void the_user_form_should_be_submitted() {
		assertEquals("Your response has been recorded.", wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("freebirdFormviewerViewResponseConfirmationMessage"))).getText());
	}

	@When("the user enters the required and non required fields based on {string}")
	public void the_user_enters_the_required_and_non_required_fields_based_on(String rowIndex) {
		List<List<String>> dataListMaster = ApplicationUtils.getInstance()
				.getListOfSheetData("form.xlsx", "required_optional");
		List<String> dataList = dataListMaster.get(Integer.parseInt(rowIndex));
		driver.findElement(By.name("entry.652836268")).sendKeys(dataList.get(0));
		driver.findElement(By.name("entry.2080829351")).sendKeys(dataList.get(1));
		driver.findElement(By.name("entry.740082629")).sendKeys(dataList.get(2));
		driver.findElement(By.name("entry.186612323")).sendKeys(dataList.get(3));
		driver.findElement(By.name("entry.1742399288")).sendKeys(dataList.get(4));
		driver.findElement(By.className("quantumWizButtonPaperbuttonLabel")).click();
	}

	@When("the user enters invalid data based on {string}")
	public void the_user_enters_invalid_data_based_on(String rowIndex) {
		List<List<String>> dataListMaster = ApplicationUtils.getInstance()
				.getListOfSheetData("form.xlsx", "invalid");
		List<String> dataList = dataListMaster.get(Integer.parseInt(rowIndex));
		driver.findElement(By.name("entry.652836268")).sendKeys(dataList.get(0));
		driver.findElement(By.name("entry.2080829351")).sendKeys(dataList.get(1));
		driver.findElement(By.name("entry.740082629")).sendKeys(dataList.get(2));
		driver.findElement(By.name("entry.186612323")).sendKeys(dataList.get(3));
		driver.findElement(By.name("entry.1742399288")).sendKeys(dataList.get(4));
	}

	@Then("the corresponding validation error should be thrown based on {string}")
	public void the_corresponding_validation_error_should_be_thrown(String rowIndex) {
		List<List<String>> dataListMaster = ApplicationUtils.getInstance()
				.getListOfSheetData("form.xlsx", "invalid");
		List<String> dataList = dataListMaster.get(Integer.parseInt(rowIndex));
		assertEquals(dataList.get(6), wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(dataList.get(5)))).getText());
	}

	
	
}
