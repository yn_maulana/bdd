package com.bdd.stepdefinitions;

import static org.junit.Assert.assertTrue;

import com.bdd.impl.Account;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BankingSteps {

	Account account;
	
	@Before
	public void initialize(){
		if(account ==null){
			account = new Account();
		}		
	}
	

	@Given("an User has no money in the account")
	public void an_User_has_no_money_in_the_account() {
		assertTrue("The balance is Zero", account.getBalance()==0);
	}
	
	@When("${int} is deposited in to the account")
	public void $_is_deposited_in_to_the_account(Integer deposit) {
		account.depositAmount(deposit);
	}
	
	@Then("the balance should be ${int}")
	public void the_balance_should_be_$(Integer newBalance) {
		System.out.println(account.getBalance());
		assertTrue("The balance is Zero", account.getBalance()==newBalance);
	}

	@Given("an User has ${int} money in the account")
	public void an_User_has_$_money_in_the_account(Integer int1) {
		account.depositAmount(100);
	}

	@When("{int}$ is withdrawn from the account")
	public void $_is_withdrawn_from_the_account(Integer withdraw) {
		account.withdrawAmount(withdraw);
	}	
	
}
