package com.bdd.stepdefinitions;

import static org.junit.Assert.assertEquals;

import com.bdd.impl.Average;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AverageSteps {
	
	float result;
	
	@Given("the user is calculating the average of two numbers")
	public void the_user_is_calculating_the_average_of_two_numbers() {
	    //
	}
	
	@When("the user enters {int} and {int}")
	public void the_user_enters_and(Integer int1, Integer int2) {
	    Average average = new Average();
	    this.result = average.findAverage(int1,int2);
	    
	}

	@Then("the result should be {int}")
	public void the_result_should_be(Integer expected) {
		assertEquals(expected.floatValue(),result,0);
	}
	

	@Then("the result should be {double}")
	public void the_result_should_be(Double expected) {
		assertEquals(expected.floatValue(), result,0);
	
	}
	
	

}
